<?php require_once "./code.php"; ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Activity s01</title>
</head>
<body>
    
<h1>Full Address</h1>
<!-- getFullAddress($country, $city, $province, $specificAddress) -->
<p><?= getFullAddress('Philippines', 'Quezon City', 'Metro Manila', '3F Caswynn Bldg., Timog Avenue') ?></p>
<p><?= getFullAddress('Philippines', 'Makati City', 'Metro Manila', '3F Enzo Bldg., Buendia Avenue') ?></p>

<h1>Letter-Based Grading</h1>
<p><?= getLetterGrade(87) ?></p>
<p><?= getLetterGrade(94) ?></p>
<p><?= getLetterGrade(74) ?></p>
</body>
</html>